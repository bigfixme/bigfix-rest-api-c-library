﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using REST.BES_Namespace;

namespace Sample
{
    class Program
    {
        static void Main(string[] args)
        {
           
            
            List<SiteSearchComponentPropertyReference> prList = new List<SiteSearchComponentPropertyReference>();

            SiteSearchComponentPropertyReference pr = new SiteSearchComponentPropertyReference();
            pr.Comparison = "Contains";
            pr.PropertyName = "Relay";
            pr.SearchText = "BES Root Server";
            pr.Relevance = "exists (if ((it does not contain \"127.0.0.1\" and it does not contain \"::1\") of name of registration server) then (name of registration server) else if (exists setting \"_BESRelay_PostResults_ParentRelayURL\" of client and exists value of setting \"_BESRelay_PostResults_ParentRelayURL\" of client as string) then (preceding text of first \"/\" of (following text of first \"//\" of (value of setting \"_BESRelay_PostResults_ParentRelayURL\" of client))) else \"BES Root Server\") whose (it as string as lowercase contains \"BES Root Server\" as lowercase)";
            prList.Add(pr);

            pr = new SiteSearchComponentPropertyReference();
            pr.Comparison = "Contains";
            pr.PropertyName = "OS";
            pr.SearchText = "Win";
            pr.Relevance = "exists (operating system) whose (it as string as lowercase contains \"Win\" as lowercase)";
            prList.Add(pr);


            CustomSiteCustomGroup cg = new CustomSiteCustomGroup();
            cg.JoinByIntersection = false;
            cg.SearchComponentPropertyReference = prList.ToArray();


            Site s = new Site();
            s.Name = "test";
            s.Description = "my description";
            s.Domain = "BFME";
            s.GlobalReadPermission = true;
            s.SiteRelevance = "true";
            s.Subscription = new SiteSubscription();
            s.Subscription.Mode = SiteSubscriptionMode.Custom;
            s.Subscription.CustomGroup = cg;



            Console.WriteLine(s.ToXML());

            Console.ReadKey();

        }
    }
}
