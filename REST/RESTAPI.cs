﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Serialization;

using REST;
using REST.BES_Namespace;
using REST.BESAPI_Namespace;

namespace REST
{
    //------------------------------------------------------------------------------
    //This section of the code is developed by Daniel H Moran for use with
    //C# applications used to access BigFix's REST API release since v9.
    //------------------------------------------------------------------------------
    //
    //

    public static class RESTAPI
    {


        /// <summary>
        /// Send a DELETE command to the specified resourceURL
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/action/51</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <returns></returns>
        static public RESTResult deleteREST(string resourceURL, Credentials credentials)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));

            


            //create a new web-client, specify creds and deal with cert, then upload BES memory stream.
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(credentials.Username, credentials.Password);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

                try
                {
                    byte[] Bresults = client.UploadData(resourceURI, "DELETE", new byte[0]);
                    return new RESTResult(Bresults);
                }
                catch (WebException ex)
                {
                    string ErrorMessage = null;
                    if (ex.Response != null)
                    {
                        using (var stream = ex.Response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            ErrorMessage = reader.ReadToEnd();
                        }
                    }
                    return new RESTResult(ex, ErrorMessage);
                }
                catch (Exception ex)
                {
                    return new RESTResult(ex);
                }
            }

        }



        /// <summary>
        /// PUT a BES object to the specified resource URL
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/actions</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <param name="bes">BES object you're posting... null is acceptable if simply posting to the resourceURL.</param>
        /// <returns></returns>
        static public RESTResult putREST(string resourceURL, Credentials credentials, BES bes)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));



            //create a new web-client, specify creds and deal with cert, then upload BES memory stream.
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(credentials.Username, credentials.Password);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

                byte[] Bresults;
                if (bes != null)
                {
                    //create a serializer from our BES object
                    var serializer = new XmlSerializer(typeof(BES));
                    string utf8;
                    using (StringWriter writer = new Utf8StringWriter())
                    {
                        serializer.Serialize(writer, bes);
                        utf8 = writer.ToString();
                    }
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(utf8);

                    try
                    {
                        Bresults = client.UploadData(resourceURI, "PUT", bytes);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage, utf8);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }
                else
                {
                    try
                    {
                        Bresults = client.UploadData(resourceURI, "PUT", new byte[0]);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }

                return new RESTResult(Bresults);
            }

        }


        /// <summary>
        /// PUT a BES object to the specified resource URL
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/actions</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <param name="bes">BES object you're posting... null is acceptable if simply posting to the resourceURL.</param>
        /// <returns></returns>
        static public RESTResult putREST(string resourceURL, Credentials credentials, BESAPI besapi)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));



            //create a new web-client, specify creds and deal with cert, then upload BES memory stream.
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(credentials.Username, credentials.Password);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

                byte[] Bresults;
                if (besapi != null)
                {
                    //create a serializer from our BES object
                    var serializer = new XmlSerializer(typeof(BESAPI));
                    string utf8;
                    using (StringWriter writer = new Utf8StringWriter())
                    {
                        serializer.Serialize(writer, besapi);
                        utf8 = writer.ToString();
                    }
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(utf8);

                    try
                    {
                        Bresults = client.UploadData(resourceURI, "PUT", bytes);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage, utf8);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }
                else
                {
                    try
                    {
                        Bresults = client.UploadData(resourceURI, "PUT", new byte[0]);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }

                return new RESTResult(Bresults);
            }

        }



        /// <summary>
        /// POST a BES object to the specified resource URL
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/actions</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <param name="bes">BES object you're posting... null is acceptable if simply posting to the resourceURL.</param>
        /// <returns></returns>
        static public RESTResult postREST(string resourceURL, Credentials credentials, BES bes)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));



            //create a new web-client, specify creds and deal with cert, then upload BES memory stream.
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(credentials.Username, credentials.Password);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

                byte[] Bresults;
                if (bes != null)
                {
                    //create a serializer from our BES object
                    var serializer = new XmlSerializer(typeof(BES));
                    string utf8;
                    using (StringWriter writer = new Utf8StringWriter())
                    {
                        serializer.Serialize(writer, bes);
                        utf8 = writer.ToString();
                    }
                    utf8 = utf8.Replace("<Target />", "");  //added to fix a rejection by Actions with no "target" (specific computers), but instead using relevance for targeting.
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(utf8);

                    try
                    {
                        Bresults = client.UploadData(resourceURI, bytes);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage, utf8);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }
                else
                {
                    try
                    {
                        Bresults = client.UploadData(resourceURI, new byte[0]);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }

                return new RESTResult(Bresults);
            }

        }


        /// <summary>
        /// POST a BES object to the specified resource URL
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/actions</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <param name="bes">BES object you're posting... null is acceptable if simply posting to the resourceURL.</param>
        /// <returns></returns>
        static public RESTResult postREST(string resourceURL, Credentials credentials, BESAPI besapi)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));


            //create a new web-client, specify creds and deal with cert, then upload BES memory stream.
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(credentials.Username, credentials.Password);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

                byte[] Bresults;
                if (besapi != null)
                {
                    //create a serializer from our BES object
                    var serializer = new XmlSerializer(typeof(BESAPI));
                    string utf8;
                    using (StringWriter writer = new Utf8StringWriter())
                    {
                        serializer.Serialize(writer, besapi);
                        utf8 = writer.ToString();
                    }
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(utf8);

                    try
                    {
                        Bresults = client.UploadData(resourceURI, bytes);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage, utf8);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }
                else
                {
                    try
                    {
                        Bresults = client.UploadData(resourceURI, new byte[0]);
                    }
                    catch (WebException ex)
                    {
                        string ErrorMessage = null;
                        if (ex.Response != null)
                        {
                            using (var stream = ex.Response.GetResponseStream())
                            using (var reader = new StreamReader(stream))
                            {
                                ErrorMessage = reader.ReadToEnd();
                            }
                        }
                        return new RESTResult(ex, ErrorMessage);
                    }
                    catch (Exception ex)
                    {
                        return new RESTResult(ex);
                    }
                }

                return new RESTResult(Bresults);
            }

        }










        /// <summary>
        /// POST a File to the specified resource URL
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/actions</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <param name="fileName">Full path to the file you wish to POST.</param>
        /// <returns></returns>
        static public RESTResult postREST(string resourceURL, Credentials credentials, string fileName)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));



            //create a new web-client, specify creds and deal with cert, then upload BES memory stream.
            using (WebClient client = new WebClient())
            {
                client.Credentials = new NetworkCredential(credentials.Username, credentials.Password);
                System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);

                byte[] Bresults;

                try
                {
                    Bresults = client.UploadFile(resourceURI, "POST", fileName);
                }
                catch (WebException ex)
                {
                    string ErrorMessage = null;
                    if (ex.Response != null)
                    {
                        using (var stream = ex.Response.GetResponseStream())
                        using (var reader = new StreamReader(stream))
                        {
                            ErrorMessage = reader.ReadToEnd();
                        }
                    }
                    return new RESTResult(ex, ErrorMessage);
                }
                catch (Exception ex)
                {
                    return new RESTResult(ex);
                }

                return new RESTResult(Bresults);
            }

        }















        /// <summary>
        /// GET data from the resourceURL specified.
        /// </summary>
        /// <param name="resourceURL">REST API Url of resource you're posting to.  Ex: http://[rootserver]:[port]/api/actions</param>
        /// <param name="Username">Console username to connect with.</param>
        /// <param name="Password">Password for the username specified.</param>
        /// <returns></returns>
        static public RESTResult getREST(string resourceURL, Credentials credentials)
        {
            Uri resourceURI;
            if (!resourceURL.ToLower().StartsWith("http")) //then pull beginning from credentials
                resourceURL = credentials.Url + resourceURL;
            if (!Uri.TryCreate(resourceURL, UriKind.Absolute, out resourceURI))
                return new RESTResult(new Exception("Invalid resource URL"));

            //resourceURL = resourceURL.Replace("https://", "http://");
            

            //Access RES and pull url
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resourceURI);
            request.Credentials = new NetworkCredential(credentials.Username, credentials.Password);


            //sending with basic authentication
            string authInfo = credentials.Username + ":" + credentials.Password;
            authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            request.Headers.Add("Authorization", "Basic " + authInfo);
        

            System.Net.ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                       
            ((HttpWebRequest)request).KeepAlive = false;
            
            try
            {
                using (WebResponse response = request.GetResponse())
                {
                    if (((HttpWebResponse)response).StatusDescription == "OK")
                    {
                        try
                        {
                            Stream stream = response.GetResponseStream();
                            return new RESTResult(stream);
                        }
                        catch (Exception ex)
                        {
                            return new RESTResult(ex);
                        }
                    }
                    else
                    {
                        return new RESTResult(new Exception(((HttpWebResponse)response).StatusDescription));
                    }
                }
            }
            catch (WebException ex)
            {
                string ErrorMessage = null;
                if (ex.Response != null)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        ErrorMessage = reader.ReadToEnd();
                    }
                }
                return new RESTResult(ex, ErrorMessage);
            }
            catch (Exception ex)
            {
                // Something more serious happened
                // like for example you don't have network access
                // we cannot talk about a server exception here as
                // the server probably was never reached
           
                return new RESTResult(ex);
            }

        }


        /// <summary>
        /// Add element to an existing array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">Original array to be added to.</param>
        /// <param name="itemToAdd">New object to add to the array.</param>
        /// <returns>Original array with added element.</returns>
        public static T[] AddItemToArray<T>(this T[] original, T itemToAdd)
        {
            T[] finalArray = new T[original.Length + 1];
            for (int i = 0; i < original.Length; i++)
            {
                finalArray[i] = original[i];
            }
            finalArray[finalArray.Length - 1] = itemToAdd;
            return finalArray;
        }

    }


    public class RESTResult
    {
        public byte[] byteArray { get; set; }
        public BES bes { get; set; }
        public BESAPI besapi { get; set; }
        public string result { get; set; }

        public bool isError { get; set; }
        public string ErrorMessage { get; set; }
        

        /// <summary>
        /// convert datastream into byteArray, bes, and result string.
        /// </summary>
        /// <param name="dataStream"></param>
        public RESTResult(Stream dataStream)
        {
            //convert to byte array
            try
            {
                byteArray = ReadFully(dataStream);
            }
            catch (Exception ex) 
            {
                isError = true;

                if (ErrorMessage == null)
                    ErrorMessage = "------------" + Environment.NewLine + "byteArray Conversion" + Environment.NewLine + ex.Message;
                else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "byteArray Conversion" + Environment.NewLine + ex.Message;
                if (ex.InnerException != null)
                {
                    Exception Iex = ex.InnerException;
                    while (Iex != null)
                    {
                        ErrorMessage += Environment.NewLine + Iex.Message;
                        Iex = Iex.InnerException;
                    }
                }
            }

            //convert into string
            try
            {
                //StreamReader reader = new StreamReader(dataStream);
                //result = reader.ReadToEnd();            

                result = System.Text.Encoding.UTF8.GetString(byteArray);
            }
            catch (Exception ex)
            {
                isError = true;

                if (ErrorMessage == null)
                    ErrorMessage = "------------" + Environment.NewLine + "result String Conversion" + Environment.NewLine + ex.Message;
                else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "result String Conversion" + Environment.NewLine + ex.Message;
                if (ex.InnerException != null)
                {
                    Exception Iex = ex.InnerException;
                    while (Iex != null)
                    {
                        ErrorMessage += Environment.NewLine + Iex.Message;
                        Iex = Iex.InnerException;
                    }
                }
            }

            //convert into bes file
            if ((result.ToLower().Trim().StartsWith("<bes") || result.ToLower().Trim().StartsWith("<?xml")) && result.Contains("xsi:noNamespaceSchemaLocation=\"BES.xsd\""))
            {
                try
                {
                    Stream stream = new MemoryStream(byteArray);

                    //create a serializer from our BES object
                    var serializer = new XmlSerializer(typeof(BES));

                    //serialize BES object into xml and into memory location
                    bes = (BES)serializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    isError = true;

                    if (ErrorMessage == null)
                        ErrorMessage = "------------" + Environment.NewLine + "BES Conversion" + Environment.NewLine + ex.Message;
                    else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "BES Conversion" + Environment.NewLine + ex.Message;
                    if (ex.InnerException != null)
                    {
                        Exception Iex = ex.InnerException;
                        while (Iex != null)
                        {
                            ErrorMessage += Environment.NewLine + Iex.Message;
                            Iex = Iex.InnerException;
                        }
                    }
                }
            }




            //convert into bes file
            if ((result.ToLower().Trim().StartsWith("<besapi") || result.ToLower().Trim().StartsWith("<?xml")) && result.Contains("xsi:noNamespaceSchemaLocation=\"BESAPI.xsd\""))
            {
                try
                {
                    Stream stream2 = new MemoryStream(byteArray);

                    //create a serializer from our BES object
                    var serializer2 = new XmlSerializer(typeof(BESAPI));

                    //serialize BES object into xml and into memory location
                    besapi = (BESAPI)serializer2.Deserialize(stream2);
                }
                catch (Exception ex)
                {
                    isError = true;

                    if (ErrorMessage == null)
                        ErrorMessage = "------------" + Environment.NewLine + "BESAPI Conversion" + Environment.NewLine + ex.Message;
                    else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "BESAPI Conversion" + Environment.NewLine + ex.Message;
                    if (ex.InnerException != null)
                    {
                        Exception Iex = ex.InnerException;
                        while (Iex != null)
                        {
                            ErrorMessage += Environment.NewLine + Iex.Message;
                            Iex = Iex.InnerException;
                        }
                    }
                }
            }



        }

        /// <summary>
        /// Convert byte array into bes and result string.
        /// </summary>
        /// <param name="bArray"></param>
        public RESTResult(byte[] bArray)
        {
            //convert to byte array
            byteArray = bArray;

            //convert into string
            try
            {
                result = System.Text.Encoding.UTF8.GetString(byteArray);
            }
            catch (Exception ex)
            {
                isError = true;

                if (ErrorMessage == null)
                    ErrorMessage = "------------" + Environment.NewLine + "result String Conversion" + Environment.NewLine + ex.Message;
                else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "result String Conversion" + Environment.NewLine + ex.Message;
                if (ex.InnerException != null)
                {
                    Exception Iex = ex.InnerException;
                    while (Iex != null)
                    {
                        ErrorMessage += Environment.NewLine + Iex.Message;
                        Iex = Iex.InnerException;
                    }
                }
            }



            //convert into bes file
            if (result.Contains("xsi:noNamespaceSchemaLocation=\"BES.xsd\""))
            {
                try
                {
                    //convert byte[] to stream which can be serialized
                    Stream stream = new MemoryStream(byteArray);

                    //create a serializer from our BES object
                    var serializer = new XmlSerializer(typeof(BES));

                    //serialize BES object into xml and into memory location
                    bes = (BES)serializer.Deserialize(stream);
                }
                catch (Exception ex)
                {
                    isError = true;

                    if (ErrorMessage == null)
                        ErrorMessage = "------------" + Environment.NewLine + "BES Conversion" + Environment.NewLine + ex.Message;
                    else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "BES Conversion" + Environment.NewLine + ex.Message;
                    if (ex.InnerException != null)
                    {
                        Exception Iex = ex.InnerException;
                        while (Iex != null)
                        {
                            ErrorMessage += Environment.NewLine + Iex.Message;
                            Iex = Iex.InnerException;
                        }
                    }
                }
            }

            //convert into besapi file
            if (result.Contains("xsi:noNamespaceSchemaLocation=\"BESAPI.xsd\""))
            {
                try
                {
                    //convert byte[] to stream which can be serialized
                    Stream stream2 = new MemoryStream(byteArray);

                    //create a serializer from our BES object
                    var serializer2 = new XmlSerializer(typeof(BESAPI));

                    //serialize BES object into xml and into memory location
                    besapi = (BESAPI)serializer2.Deserialize(stream2);
                }
                catch (Exception ex)
                {
                    isError = true;

                    if (ErrorMessage == null)
                        ErrorMessage = "------------" + Environment.NewLine + "BESAPI Conversion" + Environment.NewLine + ex.Message;
                    else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "BESAPI Conversion" + Environment.NewLine + ex.Message;
                    if (ex.InnerException != null)
                    {
                        Exception Iex = ex.InnerException;
                        while (Iex != null)
                        {
                            ErrorMessage += Environment.NewLine + Iex.Message;
                            Iex = Iex.InnerException;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Error occured but we still need to return a RESTResult
        /// </summary>
        /// <param name="ex">Exception object.</param>
        public RESTResult(Exception ex)
        {
            isError = true;


            if (ErrorMessage == null)
                ErrorMessage = "------------" + Environment.NewLine + "Error Result" + Environment.NewLine + ex.Message;
            else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "Error Result" + Environment.NewLine + ex.Message;
            if (ex.InnerException != null)
            {
                Exception Iex = ex.InnerException;
                while (Iex != null)
                {
                    ErrorMessage += Environment.NewLine + Iex.Message;
                    Iex = Iex.InnerException;
                }
            }
        }


        public RESTResult(WebException ex, string Message, string rawXml = null)
        {
            isError = true;
            
            if (ErrorMessage == null)
                ErrorMessage = "------------" + Environment.NewLine + "Error Result" + Environment.NewLine + ex.Message;
            else ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + "Error Result" + Environment.NewLine + ex.Message;
            if (ex.InnerException != null)
            {
                Exception Iex = ex.InnerException;
                while (Iex != null)
                {
                    ErrorMessage += Environment.NewLine + Iex.Message;
                    Iex = Iex.InnerException;
                }
            }

            ErrorMessage += Environment.NewLine + Message;

            if (!string.IsNullOrEmpty(rawXml))
                ErrorMessage += Environment.NewLine + "------------" + Environment.NewLine + rawXml;
        }



        /// <summary>
        /// Convert Stream to byte array.
        /// </summary>
        /// <param name="input">Data Stream</param>
        /// <returns>byte[]</returns>
        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }





        public void SaveToDisk(string path)
        {
            if (this.bes == null && this.besapi == null && this.byteArray != null) //then we have a file content... so save it to disk
            {

                File.WriteAllBytes(path, this.byteArray);
                
            }
            else
            {

                //convert to XML string
                var serializer = new XmlSerializer(typeof(BES));
                string utf8;
                using (StringWriter writer = new Utf8StringWriter())
                {
                    serializer.Serialize(writer, bes);
                    utf8 = writer.ToString();
                }
                //utf8 = utf8.Replace("<Target />", "");  //added to fix a rejection by Actions with no "target" (specific computers), but instead using relevance for targeting.



                // This text is added only once to the file. 
                if (File.Exists(path))
                    File.Delete(path);

                // Create a file to write to. 
                File.WriteAllText(path, utf8);

            }
         
        }


    }



    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }

}