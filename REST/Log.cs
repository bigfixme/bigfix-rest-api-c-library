﻿using System;
//using System.Collections.Generic;
using System.Linq;
//using System.Text;
using System.IO;
using System.Xml;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace REST
{
    public class Log
    {

        //[Conditional("DEBUG")]
        public static void DebugPrintTrace(string message)
        {
            StackTrace stackTrace = new StackTrace(true);
            StackFrame sf = stackTrace.GetFrame(1);
            Console.WriteLine("Trace "
                + sf.GetMethod().Name + " "
                + sf.GetFileName() + ":"
                + sf.GetFileLineNumber());

            Console.WriteLine(message);
        }


        /// <summary>
        /// Creates a StackFrame object locally or built from an Exception object.
        /// This gives you additional debug information like function, file name, line number, etc.
        /// </summary>
        /// <param name="ex">Exception object to build the StackFrame from.</param>
        /// <returns>StackFrame object for getting additional information from.</returns>
        public static StackFrame Stack(Exception ex = null)
        {
            StackTrace stackTrace = null;
            StackFrame sf = null;
            if (ex != null)
            {
                stackTrace = new StackTrace(ex, true);
                sf = stackTrace.GetFrame(0);
            }
            else
            {
                stackTrace = new StackTrace(true);
                sf = stackTrace.GetFrame(1);
            }
            return sf;
        }


        /// <summary>
        /// Log messages for troubleshooting and tracking purposes.
        /// </summary>
        /// <param name="msg">message to be logged</param>
        /// <param name="logicalPath">Log.Stack( [exception object] ) - we'll handle building it, but it must be built at Log.Add call time.</param>
        /// <param name="startTime">DateTime which will be tranlated into a log filename.</param>
        /// <param name="isError">Is this entry reporting an error.  (Allows for quick searching via Relevance.)</param>
        /// <param name="keepLogs">Since we cleanup the logs in this same function, how many logs should we keep before removing old ones.</param>
        static public void Add(string msg, StackFrame logicalPath, DateTime startTime, bool isError = false, int keepLogs = 15)
        {
            //output to command line so we can see what's going on.
            Console.WriteLine(msg);

            string selfLog = null;
            
            //Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)
            string logFile = AppDomain.CurrentDomain.BaseDirectory + @"\" + startTime.ToString("yyyyMMddHHmm") + ".xml";

            if (File.Exists(logFile))
                _doc.Load(logFile);
            else
            {
                var root = _doc.CreateElement("entries");
                _doc.AppendChild(root);



                try
                {

                    //time for log cleanup... only keep x number of logs
                    var files = (from file in Directory.GetFiles(Path.GetDirectoryName(logFile), "*.xml")
                                    orderby file
                                    select file).ToList();
                    files = files.Take(files.ToList().Count - keepLogs + 1).ToList();

                    if (files.Count > 0)
                    {
                        Console.WriteLine("Cleaning up the following log files:");

                        selfLog = "Cleaning up the following log files:";

                        bool cleanedSome = false;
                        foreach (string file in files)
                        {
                            if (!string.IsNullOrEmpty(file))
                            {
                                Match match = Regex.Match(Path.GetFileName(file), @"[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].xml");
                                if (match.Success)
                                {
                                    Console.WriteLine(file.Replace(AppDomain.CurrentDomain.BaseDirectory, ""));
                                    selfLog += Environment.NewLine + "\t" + file;
                                    try
                                    {
                                        cleanedSome = true;
                                        File.Delete(file);
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("Cleanup Failed:  " + ex.Message);
                                        selfLog += Environment.NewLine + "\t" + "Cleanup Failed:  " + ex.Message;
                                    }
                                }
                            }
                        }
                        if (!cleanedSome)
                        {
                            Console.WriteLine("No logs match required criteria, nothing to clean up.");
                            selfLog += Environment.NewLine + "\t" + "No logs match required criteria, nothing to clean up.";
                        }
                    }
                }
                catch (Exception ex)
                {
                    selfLog = "Files Cleanup Exception:  " + ex.Message;
                }

            }


            string message = "";
            lock (Locker)
            {
                var el = (XmlElement)_doc.DocumentElement.AppendChild(_doc.CreateElement("entry"));

                if (!string.IsNullOrEmpty(selfLog))
                {
                    //el.SetAttribute("timestamp", DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss tt"));
                    //el.SetAttribute("errorstate", "False");
                    el.SetAttribute("function", "Add");
                    el.SetAttribute("filename", "Log.cs");
                    el.SetAttribute("linenumber", "0");

                    el.AppendChild(_doc.CreateElement("timestamp")).InnerText = DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss tt");
                    el.AppendChild(_doc.CreateElement("errorstate")).InnerText = "False";

                    message = selfLog.Trim();
                    if (message.Contains("<") || message.Contains(">"))
                        message = "<![CDATA[" + message + "]]>";
                    el.AppendChild(_doc.CreateElement("message")).InnerXml = message;


                    el = (XmlElement)_doc.DocumentElement.AppendChild(_doc.CreateElement("entry"));  //clear and get it ready for a new one
                }

                //el.SetAttribute("timestamp", DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss tt"));
                //el.SetAttribute("errorstate", isError.ToString());
                if (!string.IsNullOrEmpty(logicalPath.GetMethod().Name)) el.SetAttribute("function", logicalPath.GetMethod().Name);
                if (!string.IsNullOrEmpty(logicalPath.GetFileName())) el.SetAttribute("filename", Path.GetFileName(logicalPath.GetFileName()));
                if (!string.IsNullOrEmpty(logicalPath.GetFileLineNumber().ToString())) el.SetAttribute("linenumber", logicalPath.GetFileLineNumber().ToString());

                el.AppendChild(_doc.CreateElement("timestamp")).InnerText = DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss tt");
                el.AppendChild(_doc.CreateElement("errorstate")).InnerText = isError.ToString();

                message = msg.Trim();
                if (message.Contains("<") || message.Contains(">") || message.Contains("&"))
                    message = "<![CDATA[" + message + "]]>";
                el.AppendChild(_doc.CreateElement("message")).InnerXml = message;

                
                _doc.Save(logFile);
            }
        }
        private static XmlDocument _doc = new XmlDocument();
        private static readonly object Locker = new object();


    }
}
