﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace REST
{
    class DashboardVariableValue
    {
        public object originalObject { get; set; }
        public string isDeletedSVar { get; set; }
        public string dashboardID { get; set; }
        public string serializedVariable { get; set; }
        public string lastSavedBy { get; set; }
        public string isPrivate { get; set; }
        public string dateSaved { get; set; }
        public string value { get; set; }
        public string name { get; set; }
        public string lastDateSaved { get; set; }
    }
}
