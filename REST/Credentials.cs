﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace REST
{
    public class Credentials
    {
        public bool isMaster = false;

        public string Version { get; set; }

        private string _Url = null;
        private Uri URI = null;
        public string Url { 
            get 
            {
                if (URI == null)
                    return null;
                else return URI.AbsoluteUri.TrimEnd('/');
            }
            set
            {
                Uri uriResult;
                if (Uri.TryCreate(value, UriKind.Absolute, out uriResult))  //validate it is a properly formated URL
                {
                    string tmpUrl;
                    if (uriResult.Scheme == Uri.UriSchemeHttps)  //validate it has HTTPS... if not then change it
                        tmpUrl = uriResult.AbsoluteUri;
                    else
                    {
                        tmpUrl = string.Format("https://{0}:{1}{2}", uriResult.DnsSafeHost, uriResult.Port.ToString(), uriResult.AbsolutePath);
                    }
                    if (tmpUrl.EndsWith("/api")) tmpUrl = tmpUrl.Replace("/api", "");
                    if (tmpUrl.EndsWith("/")) tmpUrl = tmpUrl.Trim('/');

                    URI = new Uri(tmpUrl);
                } 
                //else
                    //throw new ArgumentOutOfRangeException("value", "/* etc... */");
            }
        }
        public string Username { get; set; }
        public string Password { get; set; }

        public string Name { get; set; }

        public string PublicKey { get; set; }
        public string PrivateKey { get; set; }

        private bool UrlIsValid(string hostname)
        {
            bool br = false;
            try
            {
                IPHostEntry ipHost = Dns.Resolve(hostname);
                return true;
            }
            catch (SocketException se)
            {
                return false;
            }
        }
    }
}




