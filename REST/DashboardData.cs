﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using REST.BESAPI_Namespace;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.IO;

namespace REST
{
    public class DashboardData
    {
        static public List<BESAPIDashboardData> DashboardVariables = new List<BESAPIDashboardData>();


        static public BESAPIDashboardData Get(string name)
        {
            foreach (BESAPIDashboardData v in DashboardVariables)
            {
                if (v.Name.ToLower() == name.ToLower())
                {
                    return v;
                }
            }
            return null;
        }






        static public string Save(string name, object value, Credentials credentials, DateTime startTime, int debugLevel)
        {
            string overallStatus = "";
            try
            {
                BESAPIDashboardData DBV = DashboardData.Get(name);
                JavaScriptSerializer ser = new JavaScriptSerializer();

                if (DBV != null)
                {
                    //update dd value
                    Dictionary<string, string> values = ser.Deserialize<Dictionary<string, string>>(DBV.Value);
                    values["value"] = ser.Serialize(value);
                    values["serializedValue"] = ser.Serialize(values["value"]);

                    //now update the dashboard variable itself
                    DBV.Value = ser.Serialize(values);

                    //06/28/2013 17:09:50 PM | saveVariable (GuidsTable) ERROR:  
                    //Error during serialization or deserialization using the JSON JavaScriptSerializer. 
                    //The length of the string exceeds the value set on the maxJsonLength property.


                    //create a new BESAPI object and add our schedule variable to it
                    BESAPI besapi = new BESAPI();
                    besapi.Items = new object[] { DBV };
                    besapi.ItemsElementName = new REST.BESAPI_Namespace.ItemsChoiceType1[] { REST.BESAPI_Namespace.ItemsChoiceType1.DashboardData };

                    //if (debugLevel >= 10) Log.Add(XML.Get(besapi), Log.Stack(), startTime);

                    //call the post command sending in our new besapi object
                    RESTResult rr = RESTAPI.postREST("/api/dashboardvariable/Sync.ojo/" + name, credentials, besapi);
                    if (rr.isError)
                    {
                        throw new Exception(rr.ErrorMessage);
                    }
                }
                else //we need to create it
                {
                    DBV = new BESAPIDashboardData();
                    DBV.Dashboard = "Sync.ojo";
                    DBV.IsPrivate = false;
                    DBV.IsPrivateSpecified = true;
                    DBV.Name = name;
                    DBV.User = "";

                    Dictionary<string, string> values = new Dictionary<string, string>();

                    values.Add("originalObject", null);
                    values.Add("isDeletedSVar", "False");
                    values.Add("dashboardID", "Sync.ojo");
                    values.Add("serializedValue", "");
                    values.Add("lastSavedBy", "");
                    values.Add("isPrivate", "True");
                    values.Add("dateSaved", DateTime.Now.ToUniversalTime().ToString("MM/dd/yy"));
                    values.Add("value", "");
                    values.Add("name", name);
                    values.Add("lastDateSaved", DateTime.Now.ToUniversalTime().ToString("ddd MMM dd hh:mm:ss GMT-0500 yyyy")); // "Wed Apr 17 15:14:58 GMT-0500 2013");

                    values["value"] = ser.Serialize(value);
                    values["serializedValue"] = ser.Serialize(values["value"]);

                    //now update the dashboard variable itself
                    DBV.Value = ser.Serialize(values);

                    DashboardVariables.Add(DBV); //add to master list for use later

                    //create a new BESAPI object and add our schedule variable to it
                    BESAPI besapi = new BESAPI();
                    besapi.Items = new object[] { DBV };
                    besapi.ItemsElementName = new REST.BESAPI_Namespace.ItemsChoiceType1[] { REST.BESAPI_Namespace.ItemsChoiceType1.DashboardData };

                    //if (debugLevel >= 10) Log.Add(XML.Get(besapi), Log.Stack(), startTime);

                    //call the post command sending in our new besapi object
                    RESTResult rr = RESTAPI.postREST("/api/dashboardvariables/Sync.ojo", credentials, besapi);
                    if (rr.isError) throw new Exception(rr.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                Exception Iex = ex.InnerException;
                while (Iex != null)
                {
                    errorMessage += Environment.NewLine + Iex.Message;
                    Iex = Iex.InnerException;
                } 
                Log.Add("ERROR:  (DashboardData:122)" + errorMessage, Log.Stack(ex), startTime, true);
                overallStatus = "ERROR:  " + ex.Message;
            }

            return overallStatus;
        }





    }
}
