﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace REST
{
    public class XML
    {

        /// <summary>
        /// Convert an object into a basic serialized XML string.
        /// </summary>
        /// <param name="obj">Object to be serialized.</param>
        /// <returns></returns>
        static public string Get(object obj)
        {
            //convert object to xml
            XmlSerializer x = new XmlSerializer(obj.GetType());
            StringWriter textWriter = new StringWriter();
            x.Serialize(textWriter, obj);
            string xml = textWriter.ToString();
            return xml;
        }


    }
}
